from datetime import datetime
from app import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import login



class User(UserMixin, db.Model):
    # __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    
    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


class Live_collection(db.Model):
    # __tablename__ = 'live_collection'

    # id_fish = db.Column(db.Integer, autoincrement=True)
    marker = db.Column(db.String(32), index=True, primary_key=True)
    kind = db.Column(db.String(64), index=True)
    age = db.Column(db.Integer, index=True)
    pool_number = db.Column(db.String(32), index=True)
    features = db.Column(db.String(120), index=True)
    genetics = db.Column(db.String(120), index=True, unique=False)
    research = db.relationship('Research', backref='fish', lazy='dynamic')
    photo = db.relationship('Photos', backref='fish', lazy='dynamic')

    # def __init__(self, marker, kind,  pool_number, features, genetics, age, research, photo):
    #     self.marker = marker
    #     self.kind = kind
    #     self.age = age
    #     self.pool_number = pool_number
    #     self.features = features
    #     self.genetics = genetics
    #     self.research = research
    #     self.photo = photo


class Photos(db.Model):
    # __tablename__ = 'photos'

    id_photo = db.Column(db.Integer, primary_key=True, autoincrement=True)
    photo = db.Column(db.BLOB)
    fish_id = db.Column(db.Integer, db.ForeignKey('live_collection.marker'))

    # def __init__(self, *args, **kwargs):
    #     super(Photos, self).__init__(*args, **kwargs)


class Research(db.Model):
    # __tablename__ = 'research'

    id_research = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date = db.Column(db.String(32), index=True)
    mass = db.Column(db.Integer, index=True)
    maturity = db.Column(db.String(32), index=True, unique=False)
    spawning = db.Column(db.String(32), index=True, unique=False)
    phisiology = db.relationship('Phisiology', backref='research', lazy='dynamic')
    fish_id = db.Column(db.Integer, db.ForeignKey('live_collection.marker'))

    # def __init__(self, *args, **kwargs):
    #     super(Research, self).__init__(*args, **kwargs)


class Phisiology(db.Model):
    # __tablename__ = 'phisiology'

    id_phisiology = db.Column(db.Integer, primary_key=True, autoincrement=True)
    urine = db.relationship('Urine', backref='phisiology', lazy='dynamic')
    blood = db.relationship('Blood', backref='phisiology', lazy='dynamic')
    research_id = db.Column(db.Integer, db.ForeignKey('research.id_research'))

    # def __init__(self, *args, **kwargs):
    #     super(Phisiology, self).__init__(*args, **kwargs)


class Urine(db.Model):
    # __tablename__ = 'urine'

    id_urine = db.Column(db.Integer, primary_key=True, autoincrement=True)
    salinity = db.Column(db.String(32), index=True, unique=False)
    osmolality = db.Column(db.String(32), index=True, unique=False)
    phisiology_id = db.Column(db.Integer, db.ForeignKey('phisiology.id_phisiology'))
    
    # def __init__(self, *args, **kwargs):
    #     super(Urine, self).__init__(*args, **kwargs)


class Blood(db.Model):
    # __tablename__ = 'blood'

    id_blood = db.Column(db.Integer, primary_key=True, autoincrement=True)
    salinity = db.Column(db.String(32), index=True, unique=False)
    osmolality = db.Column(db.String(32), index=True, unique=False)
    triglcerides = db.Column(db.String(32), index=True, unique=False)
    general_lipids = db.Column(db.String(32), index=True, unique=False)
    cholesterol = db.Column(db.String(32), index=True, unique=False)
    beta_lipoproteins = db.Column(db.String(32), index=True, unique=False)
    total_whey_protein = db.Column(db.String(32), index=True, unique=False)
    esr = db.Column(db.String(32), index=True, unique=False)
    hemoglobin = db.Column(db.String(32), index=True, unique=False)
    phisiology_id = db.Column(db.Integer, db.ForeignKey('phisiology.id_phisiology'))

    # def __init__(self, *args, **kwargs):
    #     super(Blood, self).__init__(*args, **kwargs)


class Cryobank(db.Model):
    # __tablename__ = 'cryobank'
    id_bank = db.Column(db.Integer, primary_key=True, autoincrement=True)
    bank_number = db.Column(db.Integer)
    section = db.Column(db.Integer)
    kind = db.Column(db.String(32), index=True)
    number_of_samples = db.Column(db.String(32), index=True)
    tube_number = db.Column(db.String(32), index=True)
    native_sperm_motility = db.Column(db.String(32), index=True)
    mobility_after_defrosting = db.Column(db.String(32), index=True)
    gathering_place = db.Column(db.String(64), index=True)
    note = db.Column(db.String(120), index=True)



@login.user_loader
def load_user(id):
    return User.query.get(int(id))
