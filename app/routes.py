import os
from flask import render_template, flash, redirect, url_for
from app import app, db
from app.forms import LoginForm, Table_form_live_collection
from flask_login import current_user, login_user
from app.models import User, Live_collection, Research, Photos, Phisiology, Urine, Blood, Cryobank
from flask_login import logout_user
from flask_login import login_required
from flask import request
from werkzeug.urls import url_parse
from werkzeug.utils import secure_filename
from flask import send_from_directory
import pandas as pd
from sqlalchemy import create_engine
import csv
import sqlalchemy as sqAl



@app.route('/index')
@login_required
def index():
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    query = engine.execute('SELECT * FROM live_collection')
    items = query.fetchall()
    columns = app.config['TABLE_GENERAL']
    return render_template("index.html", title='Живая коллекция', columns=columns, items=items)


@app.route('/cryobank')
@login_required
def cryobank():
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    query = engine.execute('SELECT * FROM cryobank')
    items = query.fetchall()
    # columns = engine.execute('SELECT * FROM cryobank').keys()
    columns = app.config['TABLE_CRYOBANK']
    return render_template("index.html", title='Криобанк', columns=columns, items=items)


@app.route('/blood')
@login_required
def blood():
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    query = engine.execute('SELECT * FROM blood')
    items = query.fetchall()
    columns = app.config['TABLE_BLOOD']
    # columns = engine.execute('SELECT * FROM blood').keys()
    return render_template("index.html", title='Кровь', columns=columns, items=items)


@app.route('/urine')
@login_required
def urine():
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    query = engine.execute('SELECT * FROM urine')
    items = query.fetchall()
    columns = app.config['TABLE_URINE']
    # columns = engine.execute('SELECT * FROM urine').keys()
    return render_template("index.html", title='Урина', columns=columns, items=items)


@app.route('/research')
@login_required
def research():
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    query = engine.execute('SELECT * FROM research')
    items = query.fetchall()
    columns = app.config['TABLE_RESEARCH']
    # columns = engine.execute('SELECT * FROM research').keys()
    return render_template("index.html", title='Исследования', columns=columns, items=items)


@app.route('/')
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user.html', user=user)


def allowed_file(filename):
    return '.' in filename and filename.split('.', 1)[1] in app.config['UPLOADED_EXT_ALLOW']


@app.route('/uploads', methods=['GET', 'POST'])
@login_required
def uploads():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('Файл не загружен')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('Файл не выбран')
            return redirect(request.url)
        if allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOADS_DEFAULT_DEST'], filename))
            flash('Файл загружен')
            recognizer(os.path.join(app.config['UPLOADS_DEFAULT_DEST'], filename))
            os.remove(os.path.join(app.config['UPLOADS_DEFAULT_DEST'], filename))
    return render_template('uploads.html')


@app.route('/download', methods=['GET', 'POST'])
@login_required
def download():
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    query = engine.execute('SELECT * FROM research')
    items = query.fetchall()
    columns = app.config['TABLE_RESEARCH']
    items.encoding('utf-8')
    columns.encoding('utf-8')
    df = pd.DataFrame(data=items, columns=columns)
    df.read_sql_table('research', engine, columns=columns)
    # df.to_csv(os.path.join(app.config['DOWNLOADS_DEFAULT_DEST'], 'out.cvs'), sep=';')
    return send_from_directory(directory=app.config['DOWNLOADS_DEFAULT_DEST'], filename='out.csv')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


def recognizer(filename):
    with open(filename, 'r', encoding='utf-8') as file:
        flag = 0
        df = pd.read_csv(file, sep=';')
        columns = df.columns.tolist()
        if columns == app.config['TABLE_GENERAL_UPLOAD']:
            flag = 1
            table_reader(df, flag)
        elif columns == app.config['TABLE_RESEARCH_UPLOAD']:
            flag = 2
            table_reader(df, flag)
        elif columns == app.config['TABLE_URINE_UPLOAD']:
            flag = 3
            table_reader(df, flag)
        elif columns == app.config['TABLE_BLOOD_UPLOAD']:
            flag = 4
            table_reader(df, flag)
        


def table_reader(df, flag):
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    if flag == 0:
        pass
    elif flag == 1:
        df.to_sql('live_collection', engine, if_exists='append', index=False)
    elif flag == 2:
        df.to_sql('research', engine, if_exists='append', index=False)
    elif flag == 3:
        df.to_sql('urine', engine, if_exists='append', index=False)
    elif flag == 4:
        df.to_sql('blood', engine, if_exists='append', index=False)


def table_writer(flag):
    if flag == 1:
        pass
    elif flag == 2:
        pass
    elif flag == 3:
        pass
    elif flag == 4:
        pass
