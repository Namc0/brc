import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMINS = ['your-email@example.com']

    UPLOADS_DEFAULT_DEST = os.path.join(basedir, 'app/uploads')
    UPLOADED_EXT_ALLOW = set(['csv'])
    DOWNLOADS_DEFAULT_DEST = os.path.join(basedir, 'app/tmp')

    TABLE_GENERAL = ['Метка рыбы', 'Вид', 'Возраст', 'Номер бассейна',	'Особенности', 'Генетика']
    TABLE_RESEARCH = ['ID Исследования', 'Дата',	'Масса',	'Стадия зрелости гонад',	'Нерест', 'Метка рыбы']
    TABLE_URINE = ['ID пробы','Соленость г/л',	'Осмоляльность ммоль/кг H2O', 'ID Физиологии']
    TABLE_BLOOD = ['ID пробы','Соленость г/л',	'Осмоляльность ммоль/кг H2O', 'Триглицериды ммоль/л', 'Общие липиды г/л', 'Холестирин ммоль/л',	'Бета-липопротеиды г/л', 'Общий сывороточный белок г/л', 'Скорость оседания эритроцитов мм/ч', 'Гемоглобин г/л', 'ID Физиологии']
    TABLE_CRYOBANK = ['ID', 'Номер криобанка', 'Секция', 'Вид',	'Количество образцов',	'Номер пробирки',	'Подвижность нативной спермы %',	'Подвижность после размораживания,%',	'Место сбора',	'Примечание']

    TABLE_GENERAL_UPLOAD = ['marker', 'kind', 'age', 'pool_number',	'features', 'genetics']
    TABLE_RESEARCH_UPLOAD = ['id_research',	'date',	'mass',	'maturity',	'spawning',	'fish_id']
    TABLE_URINE_UPLOAD = ['id_urine',	'salinity',	'osmolality',	'phisiology_id']
    TABLE_BLOOD_UPLOAD = ['id_blood',	'salinity',	'osmolality',	'triglcerides',	'general_lipids',	'cholesterol',	'beta_lipoproteins',	'total_whey_protein',	'esr',	'hemoglobin',	'phisiology_id']
    TABLE_CRYOBANK_UPLOAD = ['id_bank',	'bank_number', 'section', 'kind', 'number_of_samples', 'tube_number', 'native_sperm_motility', 'mobility_after_defrosting', 'gathering_place',	'note'	]