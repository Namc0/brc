# -*- coding: utf-8 -*-
from app import app, db
from app.models import User, Live_collection, Photos, Research, Phisiology, Urine, Blood, Cryobank


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Live_collection': Live_collection, 'Photos': Photos, 'Research': Research, 'Phisiology': Phisiology, 'Urine': Urine, 'Blood': Blood, 'Cryobank': Cryobank}
